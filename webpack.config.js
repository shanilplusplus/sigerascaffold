const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');   
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {
    entry: path.resolve(__dirname, './src/index.js'),
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].[contenthash].js',
        assetModuleFilename: '[name][ext]',
        clean: true,
        },
    devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    hot: true,
    open: true,
    port: 3001,
    watchContentBase: true,
    },
    plugins: [new webpack.HotModuleReplacementPlugin(), new MiniCssExtractPlugin({ filename: "[name].[contentHash].css" }), new HtmlWebpackPlugin({
        template: "./src/index.html"
    })],
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                    },
                },
            },
            { test: /\.scss$/, use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'] },
            ],
        },
    resolve: {
    extensions: ['*', '.js', '.jsx'],
    }
};