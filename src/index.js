import React from 'react';
import "./scss/main.scss";
import ReactDOM from 'react-dom';
import sayHello from './hello';

console.log("hello world")

ReactDOM.render(
<div>
    <h1>Hello World</h1>
    <h3>{sayHello()}</h3>
    <h3>{sayHello()}</h3>
   
</div>,
document.getElementById('app')
);


module.hot.accept();